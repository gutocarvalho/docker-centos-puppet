FROM gutocarvalho/centos-systemd:7
MAINTAINER Guto Carvalho "gutocarvalho@gmail.com"

ENV PUPPET_AGENT_VERSION="1.6.2"

RUN rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm && \
    yum upgrade -y && \
    yum update -y && \
    yum install -y puppet-agent-"$PUPPET_AGENT_VERSION" && \
    yum install -y epel-release \
    yum install -y git vim-enhanced wget rsync screen bind-utils nc telnet elinks lynx bzip2 unzip tcpdump ccze htop traceroute \
    yum clean all

ENV PATH=/opt/puppetlabs/server/bin:/opt/puppetlabs/puppet/bin:/opt/puppetlabs/bin:$PATH

CMD ["/usr/sbin/init"]

COPY Dockerfile /
